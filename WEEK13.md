# Week 13: Drupal content management system (cont.)

## Drupal

* [Tasks 1-5](./WEEK12.md) should be completed as of last week.

### Task 6: updating CSS

* Head go to https://teacher.main-bvxea6i-ifdehzkxpouoe.au.platformsh.site/
* Go to `My content` (top menu) once logged in.
* Edit `Section 3...` page and add header2, header3 elements. Save the page.
* Edit `Section 3...` page and add header1 element by expecting the source. Add `<h1>Fake header</h1>` tag. Save the page.

Q: Using browser developer tools explain what happened to `h1` tag?

* Edit `Section 3...` page. Explore text formats on the bottom of the body field. 
  Switch to approproate text format and try to add `h1` tag via `view source` button.
* Edit `Section 3...` page. Add `<style>h2 {background-color: lightgrey;}</style>` tag. Save the page.

Q: What happend to backgrounds of header 2?

### Task 7: adding video

* Go to [YouTube video](https://www.youtube.com/watch?v=HeRh2w1lSrI). `Share` > `Embed` > copy HTML code.
* Go to `My content` (top menu) once logged in.
* Edit `Section 3...` page and add copied HTML to embed the video. Save the page.

## Wordpress

* The most popular content management system in the world.
* https://en-au.wordpress.org/about/
* https://whatcms.org/c/WordPress

Q: Based on the link above, what is the most popular Wordpress websites in Australia?

* Download [Great Barrier Reef report](https://elibrary.gbrmpa.gov.au/jspui/handle/11017/3951) if you haven't. Click `View/Open` button on the bottom.

### Task 8: create account

* Head go to https://wordpress.main-bvxea6i-ifdehzkxpouoe.au.platformsh.site/
* `Registration` in the top menu > Submit > `Submit`.
* Remember your username and password.
* No email verification is required.

## Task 9: add new page

* Go to `Dashboard` (click on title) once logged in > `Pages` in the right hand side menu.
* Create page: `Part 3. Performance` (based on pages 31-34 of the report).
* Add formatting for tables, lists, bold and italic text, table, make sure headers are all headers 2 or 3, links (if required).
* Add images (make sure to name images appropriately).

## Task 10: find broken links

* Go to `Dashboard` (click on title) once logged in > `Pages` in the right hand side menu.
* Create page: `Program area 4:  Supporting a high-performing organisation`and copy HTML content from [WEEK13.html](./WEEK13.html).
* Find and fix broken links either manually or using online tools.

## Task 11: insert CSS from Task 6

* Color header 2 using CSS as in task 6.

## Task 12: embed YouTube video

* Use either HTML or Wordpress YouTube block to embed video from task 7.


## Assignment

* Continue working on AT2.
