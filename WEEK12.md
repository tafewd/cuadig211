# Week 12: Drupal content management system

## Task 1: create account and download the report

Due to technical issues website is now rebuilt.

* Head go to https://teacher.main-bvxea6i-ifdehzkxpouoe.au.platformsh.site/
* `Login` in the top menu > `Create new account`.
* You can use your email for username or create new username.
* Remember your username and password.
* No email verification is required.

* Download [Great Barrier Reef report](https://elibrary.gbrmpa.gov.au/jspui/handle/11017/3951). Click `View/Open` button on the bottom.

## Task 2: edit existing page

* Go to `My content` (top menu) once logged in.
* You will find two pages: `1. Introduction` and `2. Overview`.
* Edit page `1. Introduction`. Compare formatting with the report (pages 8-11) and format website page accordingly.
  * Add formatting for lists, bold and italic text, table, make sure headers are all headers 2 or 3, links (if required).
* Add missing content if needed.

## Task 3: find broken links

* Go to `My content` (top menu) once logged in.
* You will find two pages: `1. Introduction` and `2. Overview`.
* View page `2. Overview`. The page is already formatted but it might contain broken links. 
* Find and fix broken links either manually or using online tools.
* Add missing content if needed.

## Task 4: update existing page (insert images)

* Go to `My content` (top menu) once logged in.
* You will find two pages: `1. Introduction` and `2. Overview`.
* View page `2. Overview`. The page is already formatted but there are no images.
* Cut out images from report (pages 8-12 only). and insert them into your page.
* Name images properly (descriptive name containing lowercase characters, numbers, dashes, include student number).
* Add image copyright as in report and assignment.

## Task 5: create new page

* Go to `My content` (top menu) once logged in.
* Add new page titled `3. Performance`.
* Copy content (images and text) from the report (pages 14-16 only).


## Assignment

* Continue working on AT1 and AT2 theory questions.
