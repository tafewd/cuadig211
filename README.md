# CUADIG211

## Content management systems

### Week 10

#### Drupal

* [Drupal](https://www.drupal.org/home)
* [What CMS: Drupal](https://whatcms.org/c/Drupal)
* [Drupal documentation](https://www.drupal.org/docs/user_guide/en/understanding-drupal.html)

#### Class work

* Environment: See unit's `Weekly Content > Week 10` for login instructions.
* [Download Annual Report 2021-22](https://www2.gbrmpa.gov.au/reef-authority-annual-report-2021-22)
